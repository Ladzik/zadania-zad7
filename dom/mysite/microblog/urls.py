from django.conf.urls import patterns, url
from django.views.generic import TemplateView
from django.http import HttpResponse
import views
from models import Post

urlpatterns = patterns('',
    url(r'^$', views.index, name='posty'),
    url(r'^logowanie$', views.logowanie, name='logowanie'),
    url(r'^zaloguj$', views.zaloguj, name='zaloguj'),
    url(r'^logout$', views.wyloguj, name='wyloguj'),
    url(r'^/(?P<uzytkownik>\d{50})$', views.posty, name='postyUzytkownika'),
    url(r'^dodaj$', views.nowy_post, name='nowyPost'),
    url(r'^dodajPost$', views.dodaj_post, name='dodajPost'),
    url(r'^rejestracja$', views.rejestracja, name='rejestracja'),
    url(r'^zarejestruj$', views.zarejestruj, name='zarejestruj')
)

