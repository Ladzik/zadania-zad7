# Create your views here.
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.contrib import messages
from models import Post
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


def index(request):
    posty = Post.objects.all().order_by('-pk')
    return render(request, 'microblog/index.html',{'posty': posty})

def logowanie(request):
    return render(request, 'microblog/login.html')


def zaloguj(request):
    uzytkownik = request.POST['uzytkownik']
    haslo = request.POST['haslo']
    user = authenticate(username=uzytkownik, password=haslo)
    if user is not None:
        login(request, user)
        return render(request, 'microblog/index.html')
    else:
        messages.error(request, 'Nie zalogowales sie')
	return render(request, 'microblog/login.html')


def rejestracja(request):
    return render(request, 'microblog/rejestracja.html')


def zarejestruj(request):
    uzytkownik = request.POST['uzytkownik']
    haslo = request.POST['haslo']
    user = User.objects.create_user(uzytkownik, '', haslo)
    user.save()
    messages.success(request, 'Rejestracja powiodla sie')
    return render(request, 'microblog/login.html')

def wyloguj(request):
    logout(request)
    return render(request, 'microblog/index.html')

def posty(request):
    uzytkownik = request.GET.get('uzytkownik', '')
    posty = Post.objects.filter(author=uzytkownik).order_by('-pk')
    return render(request, 'microblog/index.html')


def nowy_post(request):
    if request.user.is_authenticated():
        return render(request, 'microblog/dodaj.html')
    else:
        messages.error(request, 'Nie jestes zalogowany ')
        return render(request, 'microblog/login.html')


def dodaj_post(request):
    tytul = request.POST['tytul']
    tekst = request.POST['tekst']
    uzytkownik = request.user
    post = Post(tytul=tytul, tekst=tekst, uzytkownik=uzytkownik)
    post.save()
    return render(request, 'microblog/index.html')


