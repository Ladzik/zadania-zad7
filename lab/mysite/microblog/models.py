from django.db import models

# Create your models here.


class Post(models.Model):
    tytul = models.CharField(max_length=40)
    tekst = models.CharField(max_length=160)
    data = models.DateTimeField(auto_now=True)
    autor = models.CharField(max_length=32)

    def __unicode__(self):
        return self.tytul