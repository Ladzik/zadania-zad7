# Create your views here.
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.shortcuts import get_object_or_404
from django.contrib import messages
from models import Post

def add(request):
    if(request.method == 'POST'):
        tytul = request.POST.get('tytul', 0)
        tekst = request.POST.get('tekst', 0)
        autor = request.POST.get('autor', 0)
        entry = Post(title=tytul,message = tekst, nick=autor)
        entry.save()
        url = reverse('blog:index')
    else:
        #url = reverse('dodaj')
        return render(request, 'microblog/dodaj.html', {})
    return HttpResponseRedirect(url)

def index(request):
    entries = Post.objects.all()
    return render(request, 'microblog/wypisz.html', {'posty': entries})



