from django.conf.urls import patterns, url
from django.views.generic import TemplateView, ListView, DetailView
from django.http import HttpResponse
from models import Post
import views

urlpatterns = patterns('',
    #url(r'^$', ListView.as_view(
    #    queryset=Post.objects.order_by('-data'),
    #    context_object_name='posty',
    #    template_name="wypisz.html"
    #),

    #    name="message_list"),
    url(r'^$', views.index, name='index'),
    url(r'^dodaj/',views.add, name = 'dodaj'),
    #url(r'^dodaj/',TemplateView.as_view(template_name='dodaj.html'), name='dodaj_post')

)

#urlpatterns = patterns('',
#    url(r'^$', TemplateView.as_view(template_name='microblog/stub.html'))
#)